module Scheme

    alias SpecialFormProc = (ASTandCTX -> ASTandCTX)

    def self.all_numbers?(args : Array(SchemeItem))
        return args.all? { |a| a.type == :number }
    end

    def self.all_lists?(args : Array(SchemeItem))
        return args.all? { |a| a.type == :list || a.type == :quoted_list }
    end

    def self.all_bools?(args : Array(SchemeItem))
        return args.all? { |a| a.type == :list || a.type == :bool }
    end

    def self.expect_args(args : Array(SchemeItem), num : Int32)
        if args.size != num
            raise "Wrong number of arguments provided. Expected: #{num} Provided: #{args.size}"
        end
    end

    def self.scm_car(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "(car) Not a list!" if args[0].type != :quoted_list
        return SchemeItem.new({:quoted_list, [] of SchemeItem}) if args[0].as_list.size == 0
        return args[0].as_list[0]
    end

    def self.scm_cdr(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "(cdr) Not a list!" unless all_lists?(args)
        return SchemeItem.new({:quoted_list, [] of SchemeItem}) if args[0].as_list.size < 2
        return SchemeItem.new({:quoted_list, args[0].as_list[1..-1]})
    end

    def self.scm_cons(args : Array(SchemeItem))
        expect_args(args, 2)
        case args[1].type
        when :quoted_list
            return SchemeItem.new({:quoted_list, [ args[0] ]}) if args[1].as_list.empty?
            return SchemeItem.new({:quoted_list, args[1].as_list.unshift(args[0])})
        else
            return SchemeItem.new({:quoted_list, args})
        end
    end

    def self.scm_xcons(args : Array(SchemeItem))
        expect_args(args, 2)
        return scm_cons([args[1], args[0]])
    end

    def self.scm_make_list(args : Array(SchemeItem))
        expect_args(args, 2)
        return SchemeItem.new({:quoted_list, Array.new(args[0].as_number, args[1])})
    end

    def self.scm_append(args : Array(SchemeItem))
        raise "All arguments must be lists!" unless all_lists?(args)
        return SchemeItem.new({:quoted_list, args.reduce([] of SchemeItem) { |a,e| a + e.as_list }})
    end

    def self.scm_list(args : Array(SchemeItem))
        raise "There must be at least one argument!" unless args.size > 0
        return SchemeItem.new({:quoted_list, args})
    end

    def self.scm_list?(args : Array(SchemeItem))
        expect_args(args, 1)
        return SchemeItem.new({:bool, args[0].type == :quoted_list })
    end

    def self.scm_null?(args : Array(SchemeItem))
        expect_args(args, 1)
        SchemeItem.new({:bool, args[0].type == :quoted_list && args[0].as_list.empty?})
    end

    def self.scm_length(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "Argument must be a list." unless args[0].type == :quoted_list
        return SchemeItem.new({:number, args[0].as_list.size.to_big_i})
    end

    def self.scm_eq(args : Array(SchemeItem))
        expect_args(args, 2)
        result = args[0].type == args[1].type
        result = result && args[0].value == args[1].value
        return SchemeItem.new({:bool, result})
    end

    def self.scm_and(args : Array(SchemeItem))
        raise "All arguments must be bools!" unless all_bools?(args)
        return SchemeItem.new({:bool, args.reduce(true) { |a,e| a && e.as_bool } })
    end

    def self.scm_or(args : Array(SchemeItem))
        raise "All arguments must be bools!" unless all_bools?(args)
        return SchemeItem.new({:bool, args.reduce(false) { |a,e| a || e.as_bool } })
    end

    def self.scm_not(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "All arguments must be bools!" unless all_bools?(args)
        return SchemeItem.new({:bool, !args[0].as_bool})
    end

    def self.scm_number?(args : Array(SchemeItem))
        expect_args(args, 1)
        return SchemeItem.new({:bool, args[0].type == :number })
    end

    def self.scm_add(args : Array(SchemeItem))
        if all_numbers? args
            return SchemeItem.new(args.reduce(0.to_big_i) { |a,n| a + n.as_number })
        end
        raise "Args are not numbers!"
    end

    def self.scm_mult(args : Array(SchemeItem))
        if all_numbers? args
            return SchemeItem.new(args.reduce(1.to_big_i) { |a,n| a * n.as_number })
        end
        raise "Args are not numbers!"
    end

    def self.scm_sub(args : Array(SchemeItem))
        expect_args(args, 2)
        if all_numbers? args
            return SchemeItem.new(args[0].as_number - args[1].as_number)
        end
        raise "Args are not numbers!"
    end

    def self.scm_div(args : Array(SchemeItem))
        expect_args(args, 2)
        if all_numbers? args
            return SchemeItem.new(args[0].as_number / args[1].as_number)
        end
        raise "Args are not numbers!"
    end

    def self.scm_remainder(args : Array(SchemeItem))
        expect_args(args, 2)
        if all_numbers? args
            return SchemeItem.new(args[0].as_number % args[1].as_number)
        end
        raise "Args are not numbers!"
    end

    def self.scm_number_equal(args : Array(SchemeItem))
        if all_numbers? args
            return SchemeItem.new({:bool, args[0].as_number == args[1].as_number})
        end
        raise "Args are not numbers!"
    end

    def self.scm_number_less_than(args : Array(SchemeItem))
        expect_args(args, 2)
        if all_numbers? args
            return SchemeItem.new({:bool, args[0].as_number < args[1].as_number})
        end
        raise "Args are not numbers!"
    end

    def self.scm_number_greater_than(args : Array(SchemeItem))
        expect_args(args, 2)
        if all_numbers? args
            return SchemeItem.new({:bool, args[0].as_number > args[1].as_number})
        end
        raise "Args are not numbers!"
    end

    def self.scm_print(args : Array(SchemeItem))
        args.each { |a| print (a.type == :string ? a.as_string : a.to_s) }
        print "\n"
        return SchemeItem.new({:identifier, "nil"})
    end

    @@scm_include : SpecialFormProc
    @@scm_include = ->(code : ASTandCTX) do
        ast = code.ast
        global = code.global
        expect_args(ast.as_list, 2)
        file_path = ast.as_list[1]
        raise "Argument must be a path to a file." unless ast.as_list[1].type == :string && File.exists?(file_path.as_string) 
        result = interpret_ast(ASTandCTX.new(tag_tokens(tokenize("(begin\n" + File.read(file_path.as_string) + "\n)")), code.ctx, code.cont, global))
        return result 
    end

    @@scm_lambda : SpecialFormProc
    @@scm_lambda = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        lam_args_actual = ast.as_list[1]
        if !lam_args_actual.as_list.empty?
            lam_rest_arg = lam_args_actual.as_list[-1]
            if lam_rest_arg.as_string[0] == '&' && lam_rest_arg.as_string.size > 1
                lam_args = lam_args_actual.as_list[0..-2]
            else
                lam_rest_arg = Nil
                lam_args = lam_args_actual.as_list[0..-1]
            end
        else
            lam_rest_arg = Nil
            lam_args = lam_args_actual.as_list
        end
        lam_body = ast.as_list[2]
        new_ast = SchemeItem.new({:proc, ->(args : Array(SchemeItem)) do
            if args.size != lam_args.size && (lam_rest_arg != Nil && args.size < lam_args.size)
                raise "Wrong amount of arguments provided. Expected: #{lam_args.size} Provided: #{args.size}"
            end
            lam_ctx = ctx.dup
            lam_args.each_with_index do |e,i|
                lam_ctx[e.as_string] = args[i]
            end
            if lam_rest_arg != Nil
                lam_ctx[lam_rest_arg.as(SchemeItem).as_string[1..-1]] = SchemeItem.new({:quoted_list, args[lam_args.size..-1]})
            end
            res = interpret_ast(ASTandCTX.new(lam_body, lam_ctx, code))
            return res.ast
        end })
        return ASTandCTX.new(new_ast, ctx, code, global)
    end

    @@scm_define : SpecialFormProc
    @@scm_define = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        new_ctx = ctx
        def_name_list = ast.as_list[1]
        if def_name_list.type == :list
            def_args = SchemeItem.new({ :list , (def_name_list.as_list.size > 1 ? def_name_list.as_list[1..-1] : [] of SchemeItem)})
            def_name = def_name_list.as_list[0].as_string
            def_body = ast.as_list[2]
            lambda_ast = SchemeItem.new({:list, [SchemeItem.new({:identifier, "lambda"}), def_args, def_body] })
            global[def_name] = @@scm_lambda.call(ASTandCTX.new(lambda_ast, ctx, code, global)).ast
        else
            def_body = interpret_ast(ASTandCTX.new(ast.as_list[2], ctx, code, global)).ast
            global[def_name_list.as_string] = def_body
        end
        return ASTandCTX.new(SchemeItem.new({:identifier, "nil"}), new_ctx, code, global)
    end

    @@scm_debug : SpecialFormProc
    @@scm_debug = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        puts ""
        puts "======== DEBUG START ========"
        puts "==> CONTEXT: #{ctx}"
        puts ""
        puts "==> GLOBAL: #{global}"
        puts ""
        puts "==> AST: #{ast}"
        puts "======== DEBUG   END ========"
        puts ""
        return ASTandCTX.new(SchemeItem.new({:identifier, "nil"}), ctx, code, global)
    end

    @@scm_begin : SpecialFormProc
    @@scm_begin = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        beg_ctx  = ctx.dup
        beg_body = ast.as_list[1..-1]
        res_body = beg_body.map do |b|
            res_b = interpret_ast(ASTandCTX.new(b, beg_ctx, code, global))
            beg_ctx.merge!(res_b.ctx)
            res_b.ast
        end
        return ASTandCTX.new(res_body[-1], beg_ctx, code, global)
    end

    @@scm_let : SpecialFormProc
    @@scm_let = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        raise "Let arguments must be a list." unless ast.as_list[1].type == :list
        pp ast
        raise "Let arguments must be a list containing lists." unless all_lists?(ast.as_list[1].as_list)
        let_args = ast.as_list[1].as_list 
        let_body = ast.as_list[2]
        let_ctx = ctx.dup
        let_args.each do |a|
            var = a.as_list[0].as_string
            val = interpret_ast(ASTandCTX.new(a.as_list[1], ctx, code, global))
            let_ctx[var] = val.ast
        end
        new_ast = ast.dup
        new_ast.as_list.delete_at(1)
        return @@scm_begin.call(ASTandCTX.new(new_ast, let_ctx, code, global))
    end

    @@scm_let_star : SpecialFormProc
    @@scm_let_star = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        raise "Let arguments must be a list." unless ast.as_list[1].type == :list
        pp ast
        raise "Let arguments must be a list containing lists." unless all_lists?(ast.as_list[1].as_list)
        let_args = ast.as_list[1].as_list 
        let_body = ast.as_list[2]
        let_ctx = ctx.dup
        let_args.each do |a|
            var = a.as_list[0].as_string
            val = interpret_ast(ASTandCTX.new(a.as_list[1], let_ctx, code, global))
            let_ctx[var] = val.ast
        end
        new_ast = ast.dup
        new_ast.as_list.delete_at(1)
        return @@scm_begin.call(ASTandCTX.new(new_ast, let_ctx, code, global))
    end

    @@scm_quote : SpecialFormProc
    @@scm_quote = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        expect_args(ast.as_list[1..-1], 1)
        quote_body = ast.as_list[1]
        return ASTandCTX.new(SchemeItem.new( 
            case quote_body.type
            when :list
                {:quoted_list, quote_body.as_list.map do |e|
                    new_e = SchemeItem.new({:list, [SchemeItem.new({:identifier, "quote"}), e]})
                    @@scm_quote.call(ASTandCTX.new(new_e, ctx, code)).ast 
                end }
            when :number
                {:number, quote_body.as_number}
            when :symbol, :identifier 
                {:symbol, quote_body.as_string}
            when :string
                {:string, quote_body.as_string}
            else
                raise "Invalid quote type."
            end 
        ), ctx, code, global)
    end

    @@scm_quasiquote : SpecialFormProc
    @@scm_quasiquote = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        expect_args(ast.as_list[1..-1], 1)
        quote_body = ast.as_list[1]
        return ASTandCTX.new(SchemeItem.new( 
            if quote_body.type == :list
                if quote_body.as_list[0].type == :identifier && quote_body.as_list[0].as_string == "unquote"
                    interpret_ast(ASTandCTX.new(quote_body.as_list[1], ctx, code)).ast
                else
                    {:quoted_list, quote_body.as_list.map { |e| @@scm_quasiquote.call(ASTandCTX.new(SchemeItem.new({:list, [SchemeItem.new({:identifier,"quasiquote"}),e]}),ctx,code)).ast } }
                end
            else
                @@scm_quote.call(code).ast
            end 
        ), ctx, code, global)
    end

    @@scm_unquote : SpecialFormProc
    @@scm_unquote = ->(code : ASTandCTX) do
        new_ast = code.ast.as_list[1]
        global = code.global
        ctx = code.ctx
        return interpret_ast(ASTandCTX.new(new_ast, ctx, code, global))
    end

    def self.quoted_list_to_list(code : ASTandCTX)
        new_ast = code.ast
        global = code.global
        raise_scm_error("Item must be a quoted list!", code, BACKTRACE_STEPS) unless new_ast.type == :quoted_list
        return ASTandCTX.new(SchemeItem.new({:list, new_ast.as_list.map do |e|
            SchemeItem.new(case e.type
            when :quoted_list
                quoted_list_to_list(ASTandCTX.new(e,code.ctx,code)).ast
            when :symbol
                {:identifier, e.as_string}
            else
                e
            end)
        end}), code.ctx, code, global)
    end

    # Doesn't currently replace lists, it's mainly used for replacing identifiers and symbols
    def self.replace_scm_item(find_item : SchemeItem, repl_item : SchemeItem, ast : SchemeItem) : SchemeItem
        case ast.type
        when :list, :quoted_list
            SchemeItem.new({ast.type, ast.as_list.map { |a| replace_scm_item(find_item, repl_item, a).as(SchemeItem) }})
        else
            return (ast.type == find_item.type && ast.value == find_item.value ? repl_item : ast )
        end
    end

    @@scm_defmacro : SpecialFormProc
    @@scm_defmacro = ->(code : ASTandCTX) do
        ast = code.ast
        global = code.global
        macro_name = ast.as_list[1].as_list[0]
        macro_args = ast.as_list[1].as_list[1..-1]
        macro_body = ast.as_list[2]
        SPECIAL_FORMS[macro_name.as_string] = ->(macro_code : ASTandCTX) do
            actual_args = macro_code.ast.as_list[1..-1]
            new_ctx = macro_code.ctx.dup
            pp new_ast = macro_body.dup
            macro_args.each_with_index do |a,i|
                new_ast = replace_scm_item(a, actual_args[i], new_ast)
            end
            pp new_ast
            res = quoted_list_to_list(interpret_ast(ASTandCTX.new(new_ast, new_ctx, code, global)))
            pp res.ast
            return res
        end
        return ASTandCTX.new(code.ctx["nil"], code.ctx, code, global)
    end

    @@scm_if : SpecialFormProc
    @@scm_if = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        if ast.as_list[1..-1].size != 3
            puts ast.to_s
            raise "Missing condition or branches." 
        end
        condition = interpret_ast(ASTandCTX.new(ast.as_list[1], ctx, code, global)).ast.as_bool
        t_body = ast.as_list[2]
        f_body = ast.as_list[3]
        return (condition ? interpret_ast(ASTandCTX.new(t_body, ctx, code)) : interpret_ast(ASTandCTX.new(f_body, ctx, code)))
    end

    @@scm_cond : SpecialFormProc
    @@scm_cond = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        raise "There must be at least one condition." unless ast.as_list.size > 1
        result = SchemeItem.new({:identifier, "nil"})
        cond_body = ast.as_list[1..-1]
        cond_body.each do |e|
            condition = e.as_list[0]
            body = e.as_list[1]
            if condition.type == :identifier && condition.as_string == "else"
                return interpret_ast(ASTandCTX.new(body, ctx, code, global))
            else
                return interpret_ast(ASTandCTX.new(body, ctx, code, global)) if interpret_ast(ASTandCTX.new(condition, ctx, code, global)).ast.as_bool
            end
        end
        return ASTandCTX.new(DEFAULT_CTX["nil"], ctx, code, global)
    end

    @@scm_set : SpecialFormProc
    @@scm_set = ->(code : ASTandCTX) do
        ast = code.ast
        ctx = code.ctx
        global = code.global
        set_id = ast.as_list[1]
        set_val = interpret_ast(ASTandCTX.new(ast.as_list[2], ctx, code, global))
        global[set_id.as_string] = set_val.ast
        return ASTandCTX.new(SchemeItem.new({:identifier, "nil"}), ctx, code, global)
    end

    # A hash of Procs which act directly on the AST and context
    SPECIAL_FORMS = { "lambda" => @@scm_lambda,
                      "/\\" => @@scm_lambda,
                      "define" => @@scm_define,
                      "defmacro" => @@scm_defmacro,
                      "debug"  => @@scm_debug,
                      "begin"  => @@scm_begin,
                      "let"  => @@scm_let,
                      "let*"  => @@scm_let_star,
                      "quote"  => @@scm_quote,
                      "quasiquote"  => @@scm_quasiquote,
                      "unquote" => @@scm_unquote,
                      "if"     => @@scm_if,
                      "cond"     => @@scm_cond,
                      "include" => @@scm_include,
                      "set!"  => @@scm_set }

    # A hash of standard functions and operators
    DEFAULT_CTX = { 
                    "car"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_car(args) })),
                    "cdr"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_cdr(args) })),
                    "eq?"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_eq(args) })),
                    "and"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_and(args) })),
                    "or"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_or(args) })),
                    "not"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_not(args) })),
                    "list"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_list(args) })),
                    "list?" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_list?(args) })),
                    "null?" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_null?(args) })),
                    "cons"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_cons(args) })),
                    "xcons"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_xcons(args) })),
                    "make-list"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_make_list(args) })),
                    "length"   => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_length(args) })),
                    "append" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_append(args) })),
                    "number?" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_number?(args) })),
                    "+"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_add(args) })),
                    "*"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_mult(args) })),
                    "/"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_div(args) })),
                    "remainder"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_remainder(args) })),
                    "-"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_sub(args) })),
                    "print" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_print(args) })),
                    "="     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_number_equal(args) })),
                    "<"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_number_less_than(args) })),
                    ">"     => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_number_greater_than(args) })),
                    "nil"   => SchemeItem.new(Tuple.new(:quoted_list, [] of SchemeItem)) }
end