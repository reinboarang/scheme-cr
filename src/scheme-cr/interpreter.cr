require "./stringlib.cr"

module Scheme

    IDENTIFIER_CHARACTERS = "abcdefghijklmnopqrstuvwxyz".chars + "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars + "&'.<>=+-*/\\_!?".chars
    NUMBER_CHARACTERS = "0123456789".chars

    QUOTE_IDENTIFIERS = ["quote", "unquote", "quasiquote"]

    def self.tokenize(str : String) : Array(String)
        result = [] of String
        in_comment? : Bool = false
        in_string? : Bool = false
        in_escape? : Bool = false
        cur_token : String = ""
        str.each_char do |c|
            if !in_comment?
                if !in_string?
                    case c
                    when '(','[','\'',',','`'
                        result << c.to_s
                        cur_token = ""
                    when ')',']'
                        result << cur_token unless cur_token == ""
                        result << c.to_s
                        cur_token = ""
                    when ' ', '\n', '\t', '\r'
                        result << cur_token unless cur_token == ""
                        cur_token = ""
                    when '"'
                        in_string? = true
                        cur_token += '"'
                    when ';'
                        in_comment? = true
                    else
                        cur_token += c 
                    end
                else
                    if in_escape?
                        case c
                        when 'n'
                            cur_token += '\n'
                        when 't'
                            cur_token += '\t'
                        when 'r'
                            cur_token += '\r'
                        else
                            cur_token += c
                        end
                    else
                        case c
                        when '\\'
                            in_escape? = true
                        when '"'
                            in_string? = false
                            cur_token += '"'
                            result << cur_token
                            cur_token = ""
                        else
                            cur_token += c
                        end
                    end
                end
            else
                if c == '\n'
                    in_comment? = false
                    result << cur_token unless cur_token == ""
                    cur_token = ""
                end
            end
        end
        result << cur_token unless cur_token == ""
        result
    end

    def self.tag_tokens(tokens : Array(String)) : SchemeItem
        result = [] of SchemeItem
        lists = [] of Array(SchemeItem)
        tokens.each do |t|
            # Make sure to add quoted list if it already has at most one element
            if lists.size > 0 && lists[-1].size > 1
                fst_item = lists[-1][0]
                if fst_item.type == :identifier && QUOTE_IDENTIFIERS.includes?(fst_item.as_string)
                    lists[-2] << SchemeItem.new(Tuple.new(:list, lists[-1]))
                    lists.delete_at(-1)
                end
            end
            case t 
            when "'"
                lists << [SchemeItem.new({:identifier, "quote"})]
            when ","
                lists << [SchemeItem.new({:identifier, "unquote"})]
            when "`"
                lists << [SchemeItem.new({:identifier, "quasiquote"})]
            when "(","["
                lists << [] of SchemeItem
            when ")","]"
                if lists.size == 1
                    result << SchemeItem.new(Tuple.new(:list, lists[-1]))
                else
                    lists[-2] << SchemeItem.new(Tuple.new(:list, lists[-1]))
                    lists.delete_at(-1)
                end
            else # Literals and identifiers
                if t.chars[0] == '\'' && t.chars.size > 1 && t.chars[1..-1] && t.chars[1..-1].all? { |c| (IDENTIFIER_CHARACTERS + NUMBER_CHARACTERS).includes? c }
                    lists[-1] << SchemeItem.new({:symbol, t[1..-1]})
                elsif t.chars.all?(&.number?) || (t.chars[0] == '-' && t.chars.size > 1 && t.chars[1..-1].all?(&.number?))
                    lists[-1] << SchemeItem.new(Tuple.new(:number, t.to_big_i))
                elsif t[0] == '"' && t[-1] == '"'
                    lists[-1] << SchemeItem.new({:string, t[1..-2]})
                elsif t == "#t" || t == "#f"
                    lists[-1] << SchemeItem.new({:bool, t == "#t"})
                elsif IDENTIFIER_CHARACTERS.includes?(t.chars[0]) && (t.size == 1 || t.chars[1..-1].all? { |c| (IDENTIFIER_CHARACTERS + NUMBER_CHARACTERS).includes? c })
                    lists[-1] << SchemeItem.new(Tuple.new(:identifier, t))
                else
                    lists[-1] << SchemeItem.new(Tuple.new(:unknown, t))
                end
            end
        end
        raise "Missing parentheses" if lists.size != 1
        SchemeItem.new(Tuple.new(:list, result))
    end

    class ASTandCTX
        property ast : SchemeItem, ctx : Hash(String, SchemeItem), cont : ASTandCTX | Nil, global : Hash(String, SchemeItem)

        def initialize(@ast : SchemeItem, @ctx : Hash(String, SchemeItem), @cont : ASTandCTX | Nil = nil, @global = DEFAULT_CTX)
        end
    end

    def self.construct_trace_from_continuations(env : ASTandCTX, steps : Int32)
        cont_array = [] of String
        cur_cont = env
        (1..steps).each do |i|
            if !cur_cont.nil?
                cont_array << "#{"="*i}> #{cur_cont.ast.to_s}"
                cur_cont = cur_cont.cont
            end
        end
        return cont_array
    end

    def self.raise_scm_error(msg : String, env : ASTandCTX, steps : Int32 = 4)
        construct_trace_from_continuations(env, steps).reverse.each do |c|
            puts c + "\n\n"
        end
        raise msg
    end

    BACKTRACE_STEPS = 8

    def self.interpret_ast(code : ASTandCTX) : ASTandCTX
        ast = code.ast
        ctx = code.ctx
        cont = code.cont
        global = code.global
        case ast.type
        when :quoted_list, :proc, :number, :bool, :string, :symbol
            return ASTandCTX.new(ast, ctx, code, global)
        when :identifier
            if ctx.has_key? ast.as_string
                return interpret_ast(ASTandCTX.new(ctx[ast.as_string], ctx, code, global))
            elsif global.has_key? ast.as_string
                return interpret_ast(ASTandCTX.new(global[ast.as_string], ctx, code, global))
            else
                raise_scm_error("#{ast.as_string} is not defined!", code, BACKTRACE_STEPS)
            end
        when :list
            return ASTandCTX.new(ast, ctx, code, global) if ast.as_list.empty?
            fst_item = ast.as_list[0]
            rst_items = ast.as_list[1..-1]
            case fst_item.type
            when :identifier
                if SPECIAL_FORMS.has_key? fst_item.as_string
                    res = SPECIAL_FORMS[fst_item.as_string].call(code)
                    return interpret_ast(res)
                else
                    if ctx.has_key? fst_item.as_string
                        return ASTandCTX.new(ctx[fst_item.as_string].as_proc.call(rst_items.map { |e| interpret_ast(ASTandCTX.new(e, ctx, code, global)).ast.as(SchemeItem)}), ctx, code, global)
                    elsif global.has_key? fst_item.as_string
                        return ASTandCTX.new(global[fst_item.as_string].as_proc.call(rst_items.map { |e| interpret_ast(ASTandCTX.new(e, ctx, code, global)).ast.as(SchemeItem)}), ctx, code, global)
                    else
                        raise_scm_error("#{fst_item.as_string} is not defined!", code, BACKTRACE_STEPS)
                    end
                end
            when :proc
                return ASTandCTX.new(fst_item.as_proc.call(rst_items.map { |e| interpret_ast(ASTandCTX.new(e,ctx,code,global)).ast.as(SchemeItem)}), ctx, code,global)
            when :list
                result = interpret_ast(ASTandCTX.new(ast.as_list[0], ctx, code, global))
                if result.ast.type == :proc
                    new_ast = ast.dup
                    new_ast.as_list[0] = result.ast
                    return ASTandCTX.new(new_ast, ctx, code, global)
                else
                    return result
                end
            else
                raise_scm_error("First item in a list must be a symbol or special form unless quoted.", code, BACKTRACE_STEPS)
            end
        else
            raise_scm_error("Unknown type: #{ast.type}", code, BACKTRACE_STEPS)
        end
    end

    def self.run(code : String)
        ast = tag_tokens(tokenize("(begin\n" + code + "\n)"))
        return interpret_ast(ASTandCTX.new(ast, DEFAULT_CTX)).ast
    end

end

if !ARGV.empty?
    file = ARGV[0]
    puts Scheme.run(File.read(file)).to_s
end