require "./stdlib.cr"

module Scheme 

    def self.all_string?(args : Array(SchemeItem))
        return args.all? { |a| a.type == :list || a.type == :string }
    end

    def self.scm_string_null?(args : Array(SchemeItem))
        expect_args(args, 1)
        return SchemeItem.new(args[0].as_string.empty?)
    end

    def self.scm_string_to_list(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "Argument must be a string." unless args[0].type == :string
        return SchemeItem.new({:quoted_list, args[0].as_string.chars.map { |e| SchemeItem.new({:string, e.to_s }) } })
    end

    def self.scm_list_to_string(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "Argument must be a list." unless args[0].type == :quoted_list
        raise "List must contain only strings." unless all_string?(args[0].as_list)
        return SchemeItem.new({:string, args[0].as_list.reduce("") { |a,e| a + e.as_string } })
    end

    def self.scm_string_length(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "Argument must be a string." unless args[0].type == :string
        return SchemeItem.new({:number, args[0].as_string.size.to_big_i })
    end

    def self.scm_string_to_symbol(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "Argument must be a string." unless args[0].type == :string
        return SchemeItem.new({:symbol, args[0].as_string})
    end

    def self.scm_symbol_to_string(args : Array(SchemeItem))
        expect_args(args, 1)
        raise "Argument must be a symbol." unless args[0].type == :symbol
        return SchemeItem.new({:string, args[0].as_string})
    end

    DEFAULT_CTX.merge!({ 
                         "string-null?" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_string_null?(args) })),
                         "string->list" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_string_to_list(args) })),
                         "list->string" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_list_to_string(args) })),
                         "string-length" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_string_length(args) })),
                         "string->symbol" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_string_to_symbol(args) })),
                         "symbol->string" => SchemeItem.new(Tuple.new(:proc, ->(args : Array(SchemeItem)) { scm_symbol_to_string(args) }))
                        })
end