require "./stdlib.cr"
require "big"

module Scheme

    alias ValidSchemeType = Array(SchemeItem) | String | Bool | BigInt | Proc(Array(SchemeItem), (SchemeItem))

    struct SchemeItem 

        def initialize(item : SchemeItem)
            @value = {item.type, item.value}
        end

        def initialize(info : ValidSchemeType)
            case info
            when String
                @value = {:identifier, info}
            when BigInt 
                @value = {:number, info}
            when Proc(Array(SchemeItem), SchemeItem)
                @value = {:proc, info}
            when Array(SchemeItem)
                @value = {:list, info.clone}
            when Bool
                @value = {:bool, info}
            else
                @value = {:unknown, -1}
            end
        end

        def initialize(info : Tuple(Symbol, ValidSchemeType))
            @value = info
        end

        def value
            @value[1]
        end

        def as_number
            value.as(BigInt)
        end

        def as_string
            value.as(String)
        end

        def as_proc
            value.as(Proc(Array(SchemeItem), SchemeItem))
        end

        def as_list
            value.as(Array(SchemeItem))
        end

        def as_bool
            value.as(Bool)
        end

        def type
            @value[0]
        end

        def to_s
            return case @value[0]
            when :identifier
                as_string
            when :number
                as_number.to_s
            when :proc
                as_proc.to_s
            when :list, :quoted_list 
                if as_list.empty?
                    "nil"
                else
                    "(" + as_list.reduce("") { |a,e| a + (a == "" ? "" : " ") + e.to_s } + ")"
                end
            when :bool
                as_bool ? "#t" : "#f"
            when :string
                "\"" + as_string + "\""
            when :symbol
                "'" + as_string
            else
                pp self
                raise "Invalid type."
            end
        end

    end
end