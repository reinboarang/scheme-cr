require "./spec_helper"

describe Scheme do

  ### Tokenization
  describe "#tokenize" do
    it "tokenizes empty code" do
      Scheme.tokenize("").should eq([] of String)
    end

    it "tokenizes empty lists" do
      Scheme.tokenize("()").should eq(["(", ")"])
    end

    it "tokenizes single item lists" do
      Scheme.tokenize("(1)").should eq(["(", "1", ")"])
    end

    it "tokenizes multi-item lists" do
      Scheme.tokenize("(1 2 3)").should eq(["(","1","2","3",")"])
    end

    it "tokenizes identifiers" do
      Scheme.tokenize("(a abc foo bar)").should eq(["(","a","abc","foo","bar",")"])
    end

    it "treats newlines as whitespace" do
      Scheme.tokenize("(1 2\n3)").should eq(["(","1","2","3",")"])
      Scheme.tokenize("(abcde\nfghijk\nlmnop)").should eq(["(","abcde","fghijk","lmnop",")"])
    end

    it "tokenizes lists nested within lists" do
      Scheme.tokenize("(())").should eq(["(","(",")",")"])
      Scheme.tokenize("(a (b c (d e f) g h) i)").should eq(["(","a","(","b","c","(","d","e","f",")","g","h",")","i",")"])
    end

    it "tokenizes top-level identifiers" do
      Scheme.tokenize("abc").should eq(["abc"])
      Scheme.tokenize("a b c d").should eq(["a","b","c","d"])
    end
  end

  ### Tagging tokens
  describe "#tag_tokens" do
    it "todo" do
    end
  end

  ### Interpreting
  describe "#interpret_ast" do
    it "todo" do
    end
  end

end
