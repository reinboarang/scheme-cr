# Scheme-CR

A variant of Scheme Lisp implemented in Crystal.

## Installation

```
$ cd scheme-cr
$ crystal build src/scheme-cr.cr
```

## Usage

```
$ scheme-cr <scheme file>
```

As of right now, scheme-cr is not available as a shard, rather it must be compiled and used as a standalone program.

## Development Status

My overall goal for Scheme-CR is for it to be easy to integrate with other larger projects as a simple and light-weight scripting language. 

The following is a list of planned features for Scheme-CR:
- [x] CL-style macros
- [x] Optional function arguments
- [ ] Tail-call optimization
- [ ] Continuations
- [ ] Interface for calling functions defined in Scheme code from Crystal and vice versa
- [ ] Robust exception system

I do not intend Scheme-CR to fully comply with the standards and features laid out in the R5RS standard or any other standards. As such, there will be features that break compatibility with existing Scheme implementations (ie. CL macros) and because of this, it would be better to view and use Scheme-CR as its own language with its own goals.

## Contributors

- [reinboarang](https://gitlab.com/reinboarang) Conrad - creator, maintainer
